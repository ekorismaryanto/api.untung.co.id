<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Produk_berbayar extends REST_Controller {


	public function __construct(){
		parent::__construct();
		// $this->load->model('M_produk');
	}

	public function index_get(){
		$this->db->select('tbl_produk.*, nama_toko, foto_toko, kota_toko, alamat');
		$this->db->from('tbl_produk_berbayar');
		$this->db->join('tbl_produk', 'tbl_produk.id_produk = tbl_produk_berbayar.id_produk');
		$this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_produk.id_toko');
		$this->db->where('status_produk_berbayar', 'Aktif');
		$this->db->where('status_produk', 'verifikasi');	
		$produk = $this->db->get()->result();
		if($produk==null){
		    $this->db->select('tbl_produk.*, nama_toko, foto_toko, kota_toko, alamat ');
			$this->db->from('tbl_produk');
			$this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_produk.id_toko');
        //  	$this->db->where('id_kategori', $id_kategori);
			$this->db->where('status_produk', 'verifikasi');
			$this->db->order_by('rand()');
            $this->db->limit(6);
            $data = $this->db->get()->result();
            $this->response($data, 200);  
		} else {
		 $this->response($produk, 200);   
		}
	}



}
