<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Toko extends REST_Controller {


	public function __construct(){
		parent::__construct();
	}

	public function index_get(){
		$id_toko = $this->get('id_toko');

        if ($id_toko!=null) {
        	$this->db->where('id_toko', $id_toko);
            $toko = $this->db->get('tbl_toko')->result();
        }
        else {
            $toko = $this->db->get('tbl_toko')->result();
        }
        $this->response($toko, 200);
	}
}
