<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Raja_ongkir extends CI_Controller {

	function get_provinsi(){		

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: 6614e00d3c844abbb58ff4679bd37780"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data = json_decode($response, true);
		echo $response;
		
	}

	function get_kota($id_provinsi){		

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$id_provinsi",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: 6614e00d3c844abbb58ff4679bd37780"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data = json_decode($response, true);
		echo $response;
		
	}

	function get_pengiriman($id_kota_toko,$id_kota_pengiriman,$berat,$service)
	{
		$curl = curl_init();


		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=$id_kota_toko&destination=$id_kota_pengiriman&weight=$berat&courier=$service"	,
			// CURLOPT_POSTFIELDS => "origin=2&destination=4&weight=$berat&courier=jne",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: 6614e00d3c844abbb58ff4679bd37780"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		$a =array();

		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
				  //echo $response;
			$data = json_decode($response, true);
		} for ($k=0; $k < count($data['rajaongkir']['results']); $k++) {
			for ($l=0; $l < count($data['rajaongkir']['results'][$k]['costs']); $l++) {
				$b = $data['rajaongkir']['results'][$k]['costs'][$l]['service'];
				$c = $data['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['value'];
				$dataa = array('service' => $b,
					'cost' 	=> $c );
				array_push($a, $dataa);
			}
			$s = json_encode($a);
			echo $s;

		}

	}
	
	function get_kotaPro($id_provinsi){		

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://pro.rajaongkir.com/api/city?province=$id_provinsi",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: 7d37b74d1bcbc5d913178fdb4e841f21"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data = json_decode($response, true);
		echo $response;
	}
	
    function get_kec($id_kota){		

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=$id_kota",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: 7d37b74d1bcbc5d913178fdb4e841f21"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		$data = json_decode($response, true);
		echo $response;
	}

	// function kode_pos($q){
	
	// 	// array untuk output
	// 	$result = array();
		
	// 	// cURL
	// 	$ch = curl_init();
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	// 	curl_setopt($ch, CURLOPT_REFERER, 'http://www.posindonesia.co.id/tarif/');
	// 	curl_setopt($ch, CURLOPT_URL,'http://www.posindonesia.co.id/tarif/source/kodepos.php');
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, 'keyword='.$q);     
	// 	if(!$html = curl_exec($ch)){
			
	// 		// website sdg offline
	// 		$result["status"] = "offline";
	// 	}else{
			
	// 		// website sdg online
	// 		$result["status"] = "online";
			
	// 		// merubah data json ke array
	// 		$array = json_decode($html);
			
	// 		// lalu kita ambil data label.y untuk diolah & dimasukan ke array cell
	// 		$cell = array();
	// 		foreach($array as $anu){
	// 			$data = explode(', ', $anu->label);
	// 			$data2 = explode(' - ', $data[1]);
	// 			$cell[] = array(
	// 							"alamat" => $data[0],
	// 							"kota" => $data2[0],
	// 							"kode" => $data2[1]
	// 						);
	// 		}
			
	// 		// memasukan data dari array cell ke array result
	// 		$result["data"] = $cell;
	// 	}
	// 	curl_close($ch);
		
	// 	// output array
	// 	return $result;
	// 	echo $result;
	// 	// keyword alamat
	// 	// $alamat = 'dago bandung';
	// 	// // array
	// 	// $data = kode_pos($alamat);
	// 	// // array to json
	// 	// header('Content-Type: application/json');
	// 	// header('Access-Control-Allow-Origin: *');
	// 	// echo json_encode($data, JSON_PRETTY_PRINT);
	// }
	
	
}

/* End of file Raja_ongkir.php */
/* Location: ./application/controllers/Raja_ongkir.php */
