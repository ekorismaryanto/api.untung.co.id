<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Grub_pesan extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
		$id = $this->get('id');
    $this->db->where('id_pesanan', $id);
    $this->db->from('tbl_detail_pesan');
    $this->db->join('tbl_produk', 'tbl_produk.id_produk = tbl_detail_pesan.id_produk');
    $query = $this->db->get()->result();
    $this->response($query, 200);
  }

  public function index_post()
  {
    $date = date('Y-m-d');
    $data = array(
      'id_pesanan' 		=> $this->post('id_pesanan'),
      'grup_detail_pesanan_id' 		=> $this->post('grub_detail_pesan'),
      'estimasi_pengiriman' 	=> 'Menunggu Konfirmasi Admin',
      'resi_pengiriman'		=> 'Sedang di Proses',
      'status_barang_pesanan'		=> 'Menunggu',
      'tgl_submit'		=> $date
       );
    $insert = $this->db->insert('tbl_grup_detail_pesanan', $data);
    if ($insert) {
      $id = $this->db->insert_id();
      $this->db->where('id_grup_detail_pesanan', $id);
      $query = $this->db->get('tbl_grup_detail_pesanan')->result();
      $this->response($query, 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }
}
