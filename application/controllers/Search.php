<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Search extends REST_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('M_produk');
	}

	public function index_get(){
	 
		$data = $this->get('data');

        if ($data!=null) {
            $this->db->like('nama_produk', $data);
        	$this->db->where('status_produk', 'verifikasi');	
            $produk = $this->db->get('tbl_produk')->result();
            $this->response($produk, 200);
        }
        else {
        	$this->response(array('status' => 'fail', 'keterangan' => 'tidak ada artribut yang dimasukan'), 502);
        }
	}
}