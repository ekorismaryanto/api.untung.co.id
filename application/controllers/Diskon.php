<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Diskon extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
	$kode = $this->get('kode');
    if($kode!=null){
        $this->db->where('kode_unix_diskon', $kode);
        $query = $this->db->get('tbl_diskon')->result();
        $this->response($query, 200);
    }
    else{
        $this->response(array('status' => 'eror'), 502);
    }

  }
  
	function rr(){
	    $this->db->where('id_konsumen', $id_konsumen);
        $this->db->from('tbl_pesanan');
        $this->db->join('tbl_rekening_pembayaran', 'tbl_rekening_pembayaran.id_rekening_pembayaran = tbl_pesanan.id_rekening_pembayaran');
        $query = $this->db->get()->result();
        $this->response($query, 200);
}
	
  public function index_post()
  {
    $date = date('Y-m-d');
    $data = array(
      'id_konsumen' => $this->post('id_konsumen'),
      'nama_penerima' => $this->post('nama_penerima'),
      'alamat_penerima' => $this->post('alamat_penerima'),
      'provinsi_penerima' => $this->post('provinsi_penerima'),
      'kota_penerima' => $this->post('kota_penerima'),
      'nohp_penerima' => $this->post('nohp_penerima'),
      'catatan' => $this->post('catatan'),
      'total_pesanan' => $this->post('total_pesanan'),
      'total_pengiriman' => $this->post('total_pengiriman'),
      'id_rekening_pembayaran' => $this->post('id_rekening_pembayaran'),
      'status_pesanan'    => 'menunggu',
      'tgl_pesan'         => $date
       );
    $insert = $this->db->insert('tbl_pesanan', $data);
    if ($insert) {
      $id_pesanan = $this->db->insert_id();
      $data_pesanan = array('id_pesanan' => $id_pesanan );
      $this->response($data_pesanan, 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }
  
  function index_put() {
       $id_pesanan = $this->put('id_pesanan');
          $data = array(
          'file_konfirmasi' => $this->put('foto'),
          'status_pesanan'    => 'konfirmasi'
           );
         $this->db->where('id_pesanan', $id_pesanan);
         $update = $this->db->update('tbl_pesanan', $data);
         if ($update) {
             $this->response($data, 200);
         } else {
             $this->response(array('status' => 'fail', 502));
         }
     }
  
  
}
