<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');


defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Kategori extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
		$id_kategori = $this->get('id_kategori_produk');
        if ($id_kategori == '') {
            $kategori = $this->db->get('tbl_kategori_produk')->result();
        } else {
            $this->db->where('id_kategori_produk', $id_kategori);
            $kategori = $this->db->get('tbl_kategori_produk')->result();
        }
        $this->response($kategori, 200);
	}

	// public function index_post()
	// {
	// 	  $data = array('nama'        => $this->post('nama'));
 //        $insert = $this->db->insert('tbl_test_api', $data);
 //        if ($insert) {
 //            $this->response($data, 200);
 //        } else {
 //            $this->response(array('status' => 'fail', 502));
 //        }
	// }

	  // // update data mahasiswa
   //  function index_put() {
   //      $id_api = $this->put('id_api');
   //      $data = array('nama'    => $this->put('nama'));
   //      $this->db->where('id_api', $id_api);
   //      $update = $this->db->update('tbl_test_api', $data);
   //      if ($update) {
   //          $this->response($data, 200);
   //      } else {
   //          $this->response(array('status' => 'fail', 502));
   //      }
   //  }

    //  // delete mahasiswa
    // function index_delete() {
    //     $id_api = $this->delete('id_api');
    //     $this->db->where('id_api', $id_api);
    //     $delete = $this->db->delete('tbl_test_api');
    //     if ($delete) {
    //         $this->response(array('status' => 'success'), 201);
    //     } else {
    //         $this->response(array('status' => 'fail', 502));
    //     }
    // }

}
