<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Service extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

    public function index_get(){
	$id_konsumen = $this->get('id_konsumen');
    if($id_konsumen!=null) {
            $this->db->where('id_konsumen', $id_konsumen);	
            $data = $this->db->get('tbl_service')->result();
            $this->response($data, 200);
        }
        else {
          $this->response(array('status' => 'fail', 502));
        }
	}

  public function index_post()
  {
    $date = date('Y-m-d');
    $data = array(
      'id_konsumen' => $this->post('id'),
      'nama_service' => $this->post('nama'),
      'deskripsi_service' => $this->post('diskripsi'),
      'tanggal_service' => $this->post('date'),
      'provinsi_service' => $this->post('prof'),
      'kota_service' => $this->post('kab'),
      'kabupaten_service' => $this->post('kec'),
      'no_telpon_service' => $this->post('hp'),
      'alamat_lengkap_service' => $this->post('alamat'),
      'tanggal_service_submit' => $date,
      'status_service' => 'Aktif',
      'flag_service_pesanan' => '0',
      'pic_1' => $this->post('foto1'),
      'pic_2' => $this->post('foto2'),
      'pic_3' => $this->post('foto3'),
      'pic_4' => $this->post('foto4'),
       );
    $insert = $this->db->insert('tbl_service', $data);
    if ($insert) {
      $id_service = $this->db->insert_id();
      $id = array('id' => $id_service );
      $this->response($id, 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }
  
  function index_put() {
    $id_service = $this->put('id_service');
    $ekek = array(
      'status_service' => 'Tidak Aktif'
      );
    $this->db->where('id_service', $id_service);
    $update = $this->db->update('tbl_service', $ekek);
     if ($update) {
        $this->db->where('id_service', $id_service);	
        $data = $this->db->get('tbl_service')->result();
        $this->response($data, 200);
     } else {
         $this->response(array('status' => 'fail'), 502);
     }
 }
  
  
}
