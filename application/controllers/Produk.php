<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Produk extends REST_Controller {

	public function __construct(){
		parent::__construct();
		// $this->load->model('M_produk');
	}

	public function index_get(){
	    $limit =  $this->get('limit');
	    $page = $this->get('page');
	    $number = 6;
	    $offset = $number * ($page - 1);
		$id_produk = $this->get('id_produk');
		$id_kategori = $this->get('id_kategori');
		$id_toko = $this->get('id_toko');

		if ($id_kategori!=null) {
			$this->db->select('tbl_produk.*, nama_toko, foto_toko, kota_toko, alamat ');
			$this->db->from('tbl_produk');
			$this->db->join('tbl_toko', 'tbl_toko.id_toko = tbl_produk.id_toko');
         	$this->db->where('id_kategori', $id_kategori);
			$this->db->where('status_produk', 'verifikasi');
        	if($page!=null){
			   $this->db->limit($number, $offset);
			   $produk = $this->db->get()->result();
			   $this->response($produk, 200);
        	} else {
				$this->response(array('status' => 'fail', 'keterangan' => 'tidak ada artribut yang dimasukan'), 502);
			}
         } 
        else {
        	$this->response(array('status' => 'fail', 'keterangan' => 'tidak ada artribut yang dimasukan'), 502);
        }
	}
}
