<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Pesanan_toko extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

  public function index_post()
  {
    $id_toko = $this->post('id_toko');
    $date = date('Y-m-d');
    $date_tempo = date('Y-m-d', strtotime($date. ' + 2 days'));
    $data = array(
      'id_pembayaran_pesanan' => $this->post('id_pembayaran_pesanan'),
      'id_toko' => $id_toko,
      'id_konsumen' => $this->post('id_konsumen'),
      'total_pesanan' => $this->post('total_pesanan'),
      'total_pengiriman' => $this->post('total_pengiriman'),
      'status_pesanan' => 'Pesanan Baru',
      'tgl_pesan' => $date,
      'tgl_tempo_pembayaran' => $date_tempo,
       );
    $insert = $this->db->insert('tbl_pesanan', $data);
    if ($insert) {
      $id = $this->db->insert_id();
      $respon = array(
        "id_pesanan" => $id,
        "id_toko" => $id_toko
      );
      $this->response($respon , 200);
    } else {
      $this->response(array('status' => 'fail', 502));
    }
  }
  
  public function index_get(){
		$id = $this->get('id');
    $this->db->where('id_pembayaran_pesanan', $id);
    $data = $this->db->get('tbl_pesanan')->result();
    $this->response($data, 200);
	}

}
