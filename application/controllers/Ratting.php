<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Ratting extends REST_Controller {


	public function __construct(){
		parent::__construct();
	}

    public function index_get(){
		$id_produk = $this->get('id_produk');
   
        $this->db->select('id_rating,tbl_konsumen.id_konsumen, rating');
        $this->db->from('tbl_konsumen');
        $this->db->join('tbl_rating', 'tbl_konsumen.id_konsumen = tbl_rating.id_konsumen');
        $this->db->where('tbl_rating.id_produk', $id_produk);
        $review = $this->db->get()->result();
        $this->response($review, 200);
        
        
	}
	
	
    public function index_post(){
        $date = date('Y-m-d');
        $id_konsumen = $this->post('id_konsumen');
        $id_produk = $this->post('id_produk');
        $data = array(
          'id_produk' => $id_produk,
          'id_konsumen' => $id_konsumen,
          'rating' => $this->post('ratting')
           );
        $this->db->where('id_konsumen', $id_konsumen);
        $this->db->where('id_produk', $id_produk);
        $query = $this->db->get('tbl_rating')->result(); 
        if ($query != null){
              $this->response($query, 409);
            }else{
             $insert = $this->db->insert('tbl_rating', $data);
                if ($insert) {
                  $this->response(array('status' => 'ok', 200));
                } else {
                  $this->response(array('status' => 'fail', 502));
                }  
            }
        
  }

     public function index_put() {
          $id = $this->put('id_rating');
              $data = array(
              'rating' => $this->put('ratting')
              );
             $this->db->where('id_rating', $id);
             $update = $this->db->update('tbl_rating', $data);
             if ($update) {
                 $this->response(array('status' => 'ok', 200));
             } else {
                 $this->response(array('status' => 'fail', 502));
             }
         }
}
