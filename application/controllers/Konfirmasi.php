<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi extends CI_Controller {
    	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url')); 
	}

	public function index()
	{
      header('Access-Control-Allow-Origin: *');
        $target_path = "/home/untungco/public_html/upload_image/konsumen_konfirmasi/";
         
        $target_path = $target_path . basename( $_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo basename( $_FILES['file']['name']);
        } else {
        echo $target_path;
            echo "Maaf Terjadi Kesalahan Saat Upload Data!";
        }
    }
    
    public function konfirmasiPesananService()
	{
      header('Access-Control-Allow-Origin: *');
        $target_path = "/home/untungco/public_html/upload_image/konsumen_konfir_service/";
        $target_path = $target_path . basename( $_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo basename( $_FILES['file']['name']);
        } else {
        echo $target_path;
            echo "Maaf Terjadi Kesalahan Saat Upload Data!";
        }
    }
    
     public function uploadImageService()
	{
      header('Access-Control-Allow-Origin: *');
        $target_path = "/home/untungco/public_html/upload_image/img_service/";
        $target_path = $target_path . basename( $_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo basename( $_FILES['file']['name']);
        } else {
        echo $target_path;
            echo "Maaf Terjadi Kesalahan Saat Upload Data!";
        }
    }
    
     public function uploadImageKonsumenProfil()
	{
      header('Access-Control-Allow-Origin: *');
        $target_path = "/home/untungco/public_html/upload_image/konsumen_profil/";
        $target_path = $target_path . basename( $_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo basename( $_FILES['file']['name']);
        } else {
        echo $target_path;
            echo "Maaf Terjadi Kesalahan Saat Upload Data!";
        }
    }
}
/* End of file Konfirmasi.php */
/* Location: ./application/modules/exp/controllers/Konfirmasi.php */
