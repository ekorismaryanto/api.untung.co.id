<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Konsumen extends REST_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
	}

	public function index_get(){
	$email_konsumen = $this->get('email_konsumen');
    $password = md5($this->get('password_konsumen'));
        if ($email_konsumen !=null && $password!=null) {
            $this->db->where('email_konsumen', $email_konsumen);
            $this->db->where('password_konsumen', $password);
            $query = $this->db->get('tbl_konsumen')->result();
            $this->response($query, 200);
        } else {
            $this->db->where('email_konsumen', 'kosong');
            $this->db->where('password_konsumen', 'kosong');
            $query = $this->db->get('tbl_konsumen')->result(); 
            $this->response($query, 404);
        }
	}
	
	public function index_post(){
	$email = $this->post('email_konsumen');
	$password = md5($this->post('password_konsumen'));
	$date = date('Y-m-d h:i:s');
	$data = array(
      'nama_konsumen' => $this->post('nama_konsumen'),
      'first_name' => $this->post('first_name'),
      'last_name' => $this->post('last_name'),
      'email_konsumen' => $this->post('email_konsumen'),
      'profile_url' => $this->post('profile_url'),
      'oauth_uid' => $this->post('oauth_uid'),
      'oauth_provider' => $this->post('provider'),
      'gender' => $this->post('gender'),
      'password_konsumen' => $password,
      'tanggal_daftar' => $date
       );
          $this->db->where('email_konsumen', $email);
          $konsumen = $this->db->get('tbl_konsumen')->result(); 
         if ($konsumen != null){
             $this->response($konsumen, 409);
         } else {
             $insert = $this->db->insert('tbl_konsumen', $data);
            if ($insert) {
              $id_konsumen = $this->db->insert_id();
              $this->db->where('id_konsumen', $id_konsumen);
              $query = $this->db->get('tbl_konsumen')->result();
              $this->response($query, 200);
            } else {
              $this->response($data, 502);
            }  
         }
	}
	
	function index_put() {
       $id = $this->put('id');
          $data = array(
          'nama_konsumen' => $this->put('nama'),
          'first_name' => $this->put('first'),
          'last_name' => $this->put('last'),
          'no_hp_konsumen' => $this->put('hp'),
          'gender' => $this->put('gender'),
          'profile_url' => $this->put('profile_url')
           );
         $this->db->where('id_konsumen', $id);
         $update = $this->db->update('tbl_konsumen', $data);
         if ($update) {
            $this->db->where('id_konsumen', $id);
            $konsumen = $this->db->get('tbl_konsumen')->result(); 
            $this->response($konsumen, 200);
         } else {
             $this->response($data, 502);
         }
     }
  

}
