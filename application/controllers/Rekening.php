<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-API-KEY');
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Rekening extends REST_Controller {

	public function __construct(){
		parent::__construct();
	}
	public function index_get(){
		$rek = $this->db->get('tbl_rekening_pembayaran')->result();
		$this->response($rek);
	}
       	
}
